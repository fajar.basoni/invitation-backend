const { validationResult } = require('express-validator');
const httpExceptionUtils = require('../utils/httpException');

class CommonMessage {
  Failed = "Failed";
  Success = "Success";
  NotFound = "Not Found";
  Error = "Something Went Wrong";
  UnableLogin = "Unable to Login";
  IncorrectPassword = "Incorrect Password";
}

class CommonConstant {
  InvitationType = {
    Email : 1,
    Wa : 2,
    Sms : 3
  }

  InvitationStatus = {
    Draft : 1,
    Send : 2,
    Deliver : 3,
    Read : 4 
  }

  ResponseStatus = {
    Participate : 1,
    NotParticipate : 2
  }
}

const multipleColumnSet = (object) => {
  if (typeof object != 'object') {
    throw new Error('Invalid Input');
  }

  const keys = Object.keys(object);
  const values = Object.values(object);

  columnSet = keys.map(key => `${key} = ?`).join(', ');

  return {
    columnSet,
    values
  }
}

const checkValidation = (req) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new httpExceptionUtils(400, 'Validation faild', errors);
  }
}

const hashPassword = async (req) => {
  if (req.body.password) {
    req.body.password = await bcryptjs.hash(req.body.password, 8);
  }
}

const generateText = async ({recipient_name, text_message}) => {
  let result = text_message;
  return result.replace('[[0]]', recipient_name);
}

const generateActionUrl = async ({recipient_contact, text_message, type}) => {
  const constant = new CommonConstant;
  let url = `mailto:${recipient_contact}?subject=No-Reply-Email&body=${encodeURIComponent(text_message)}`;

  if (type === constant.InvitationType.Wa) {
    url = `https://api.whatsapp.com/send?phone=${recipient_contact}&text=${encodeURIComponent(text_message)}`;
  } 
  
  return url
}

module.exports = {
  CommonMessage,
  CommonConstant,
  multipleColumnSet,
  checkValidation,
  hashPassword,
  generateText,
  generateActionUrl
}