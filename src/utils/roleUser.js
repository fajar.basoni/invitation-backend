const roleUser = {
  admin: 'Admin',
  superUser: 'SuperUser',
  general: 'General'
}

module.exports = roleUser;