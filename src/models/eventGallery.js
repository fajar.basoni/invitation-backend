const executeQuery = require('../db/connection');
const { multipleColumnSet } = require('../utils/common');
const roleUser = require('../utils/roleUser');

class EventGalleryModel {
  tableName = 'event_gallery';

  find = async (params = {}) => {
    let sqlQuery = `SELECT * FROM ${this.tableName}`;
    if (!Object.keys(params).length) {
      return await executeQuery(sqlQuery);
    }

    const { columnSet, values } = multipleColumnSet(params);
    sqlQuery += ` WHERE ${columnSet}`;
    return await executeQuery(sqlQuery, [...values]);
  }

  findOne = async (params) => {
    const { columnSet, values } = multipleColumnSet(params);
    const sqlQuery = `SELECT * FROM ${this.tableName} WHERE ${columnSet}`;
    const result = await executeQuery(sqlQuery, [...values]);
    return result[0];
  }

  create = async ({ id_event,
    gallery_name,
    gallery_description,
    gallery_photo }) => {
    const sqlQuery = `INSERT INTO ${this.tableName}
      (id_event,
      gallery_name,
      gallery_description,
      gallery_photo)
      VALUES
      (?,?,?,?);`;

    const result = await executeQuery(sqlQuery, [id_event,
      gallery_name,
      gallery_description,
      gallery_photo]);

    const affectedRows = result ? result.affectedRows : 0;
    return affectedRows;
  }

  update = async (params, id) => {
    const { columnSet, values } = multipleColumnSet(params);
    const sqlQuery = `UPDATE ${this.tableName} SET ${columnSet} WHERE id = ?`;
    const result = await executeQuery(sqlQuery, [...values, id]);
    return result;
  }

  delete = async (id) => {
    const sqlQuery = `DELETE FROM ${this.tableName} WHERE id = ${id}`;
    const result = await executeQuery(sqlQuery, [id]);
    const affectedRows = result ? result.affectedRows : 0;
    return affectedRows;
  }
}

module.exports = new EventGalleryModel;