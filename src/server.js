const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv');
const httpExceptionUtil = require('./utils/httpException');
const errorMiddleware = require('./middlewares/error');

//routes
const userRouter = require('./routes/user');
const masterInvitationStatusRouter = require('./routes/masterInvitationStatus');
const masterInvitationTypeRouter = require('./routes/masterInvitationType');
const masterResponseStatusRouter = require('./routes/masterResponseStatus');
const userInvitationRouter = require('./routes/userInvitation');
const eventRouter = require('./routes/event');

//express
const app = express();

//init environment
dotenv.config();

// parse requests of content-type: application/json
// parses incoming requests with JSON payloads
app.use(express.json());

// enabling cors for all requests by using cors middleware
app.use(cors());

// Enable pre-flight
app.options("*", cors());
const port = Number(process.env.PORT || 3001);

// Register endpoint
app.use(`/api/v1/users`, userRouter);
app.use(`/api/v1/invitation_status`, masterInvitationStatusRouter);
app.use(`/api/v1/invitation_types`, masterInvitationTypeRouter);
app.use(`/api/v1/response_status`, masterResponseStatusRouter);
app.use(`/api/v1/invitations`, userInvitationRouter);
app.use(`/api/v1/events`, eventRouter);

// 404 error
app.all('*', (req, res, next) => {
  const err = new httpExceptionUtil(404, `Endpoint Not Found`);
  next(err);
});

// Error middleware
app.use(errorMiddleware);

// starting the server
app.listen(port, () => console.log(`🚀 Server running on port ${port}!`));

module.exports = app;