const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const httpExceptionUtil = require('../utils/httpException');
const userModel = require('../models/user.js');
dotenv.config();

const auth = (...roles) => {
  return async function (req, res, next) {
    try {
      const authHeader = req.headers.authorization;
      const bearer = 'Bearer ';

      if (!authHeader || !authHeader.startsWith(bearer)) {
        throw new httpExceptionUtil(401, 'Access denied. No credentials sent!');
      }

      const token = authHeader.replace(bearer, '');
      const secretKey = process.env.SECRET_JWT || "";

      //verify token
      const decoded = jwt.verify(token, secretKey);
      const user = await userModel.findOne({ id: decoded.id });

      if (!user) {
        throw new httpExceptionUtil(401, 'Authentication Failed!');
      }

      // check if the current user is the owner user
      const ownerAuthorized = req.params.id == user.id;

      // if the current user is not the owner and
      // if the user role don't have the permission to do this action.
      // the user will get this error
      if (!ownerAuthorized && roles.length && !roles.includes(user.role)) {
        throw new httpExceptionUtil(401, 'Unauthorized');
      }

      // if has permissions
      req.currentUser = user;
      next();
    } catch (err) {
      err.status = 401;
      next(err);
    }
  }
}

const authCommon = (...roles) => {
  return async function (req, res, next) {
    try {
      const authHeader = req.headers.authorization;
      const bearer = 'Bearer ';

      if (!authHeader || !authHeader.startsWith(bearer)) {
        throw new httpExceptionUtil(401, 'Access denied. No credentials sent!');
      }

      const token = authHeader.replace(bearer, '');
      const secretKey = process.env.SECRET_JWT || "";

      //verify token
      const decoded = jwt.verify(token, secretKey);
      const user = await userModel.findOne({ id: decoded.id });

      if (!user) {
        throw new httpExceptionUtil(401, 'Authentication Failed!');
      }

      // if the user role don't have the permission to do this action.
      // the user will get this error
      if (roles.length && !roles.includes(user.role)) {
        throw new httpExceptionUtil(401, 'Unauthorized');
      }

      // if has permissions
      req.currentUser = user;
      next();
    } catch (err) {
      err.status = 401;
      next(err);
    }
  }
}

module.exports = {
  auth,
  authCommon
};