const { body } = require('express-validator');

const createMasterInvitationTypeSchema = [
  body('invitation_type_code')
    .exists()
    .withMessage('invitation_type_code is required'),
  body('invitation_type_name')
    .exists()
    .withMessage('invitation_type_name is required'),
];

const updateMasterInvitationTypeSchema = [
  body('invitation_type_code')
    .exists()
    .withMessage('invitation_type_code is required'),
  body('invitation_type_name')
    .exists()
    .withMessage('invitation_type_name is required'),
];

module.exports = {
  createMasterInvitationTypeSchema,
  updateMasterInvitationTypeSchema
}