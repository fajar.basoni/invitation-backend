const { body } = require('express-validator');

const createMasterInvitationStatusSchema = [
  body('invitation_status_code')
    .exists()
    .withMessage('invitation_status_code is required'),
  body('invitation_status_name')
    .exists()
    .withMessage('invitation_status_name is required'),
];

const updateMasterInvitationStatusSchema = [
  body('invitation_status_code')
    .exists()
    .withMessage('invitation_status_code is required'),
  body('invitation_status_name')
    .exists()
    .withMessage('invitation_status_name is required'),
];

module.exports = {
  createMasterInvitationStatusSchema,
  updateMasterInvitationStatusSchema
}