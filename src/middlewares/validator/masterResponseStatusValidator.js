const { body } = require('express-validator');

const createMasterResponseStatusSchema = [
  body('response_status_code')
    .exists()
    .withMessage('response_status_code is required'),
  body('response_status_name')
    .exists()
    .withMessage('response_status_name is required'),
];

const updateMasterResponseStatusSchema = [
  body('response_status_code')
    .exists()
    .withMessage('response_status_code is required'),
  body('response_status_name')
    .exists()
    .withMessage('response_status_name is required'),
];

module.exports = {
  createMasterResponseStatusSchema,
  updateMasterResponseStatusSchema
}