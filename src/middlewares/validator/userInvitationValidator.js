const { body } = require('express-validator');
const role = require('../../utils/roleUser');
const { CommonConstant } = require('../../utils/common')
const constant = new CommonConstant;

const createUserInvitationSchema = [
  body('username')
    .exists()
    .withMessage('username is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('full_name')
    .exists()
    .withMessage('full_name is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('id_user')
    .exists()
    .withMessage('id_user is required')
    .isNumeric()
    .withMessage('Must be a numeric'),
  body('id_event')
    .exists()
    .withMessage('id_event is required')
    .isNumeric()
    .withMessage('Must be a numeric'),
  body('recipient_name')
    .exists()
    .withMessage('full_name is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('recipient_contact')
    .exists()
    .withMessage('recipient_contact is required')
    .custom(a => {
      if (body('type').equals(constant.InvitationType.Email)) {
        if (body('recipient_contact').isEmail()) {
          return true;
        } 
        return false;
      } else if (body('type').equals(constant.InvitationType.Wa)) {
        if (body('recipient_contact').isMobilePhone()){
          return true;
        }
        return false;
      }
    }),
  body('text_message')
    .exists()
    .withMessage('text_message is required')
];

const updateUserInvitationSchema = [
  body('username')
    .exists()
    .withMessage('username is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('full_name')
    .exists()
    .withMessage('full_name is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('id_user')
    .exists()
    .withMessage('id_user is required')
    .isNumeric()
    .withMessage('Must be a numeric'),
  body('id_event')
    .exists()
    .withMessage('id_event is required')
    .isNumeric()
    .withMessage('Must be a numeric'),
  body('recipient_name')
    .exists()
    .withMessage('full_name is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('recipient_contact')
    .exists()
    .withMessage('recipient_contact is required')
    .custom(a => {
      if (body('type').equals(constant.InvitationType.Email)) {
        if (body('recipient_contact').isEmail()) {
          return true;
        } 
        return false;
      } else if (body('type').equals(constant.InvitationType.Wa)) {
        if (body('recipient_contact').isMobilePhone()){
          return true;
        }
        return false;
      }
    }),
  body('text_message')
    .exists()
    .withMessage('text_message is required')
];

module.exports = {
  createUserInvitationSchema,
  updateUserInvitationSchema
}