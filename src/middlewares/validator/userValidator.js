const { body } = require('express-validator');
const role = require('../../utils/roleUser');

const createUserSchema = [
  body('username')
    .exists()
    .withMessage('username is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('full_name')
    .exists()
    .withMessage('full_name is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('email')
    .exists()
    .withMessage('email is required')
    .isEmail()
    .withMessage('Must be a valid email')
    .normalizeEmail(),
  body('phone_number')
    .exists()
    .withMessage('phone_number is required')
    .isMobilePhone()
    .withMessage('Must be a valid phone number'),
  body('role')
    .optional()
    .isIn(role.admin, role.superUser, role.general)
    .withMessage('invalid role type'),
  body('password')
    .exists()
    .withMessage('password is required')
    .notEmpty()
    .isLength({min: 8})
    .withMessage('password must contains 8 chars'),
  body('confirm_password')
    .exists()
    .custom((value, {req}) => value == req.body.password)
    .withMessage('confirm_password field must have the same value as the password field')
];

const updateUserSchema = [

  body('username')
    .exists()
    .withMessage('username is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('full_name')
    .exists()
    .withMessage('full_name is required')
    .isLength({ min: 3 })
    .withMessage('Must be at least 3 chars long'),
  body('email')
    .exists()
    .withMessage('email is required')
    .isEmail()
    .withMessage('Must be a valid email')
    .normalizeEmail(),
  body('phone_number')
    .exists()
    .withMessage('phone_number is required')
    .isMobilePhone()
    .withMessage('Must be a valid phone number'),
  body('role')
    .optional()
    .isIn(role.admin, role.superUser, role.general)
    .withMessage('invalid role type'),
  body('password')
    .exists()
    .withMessage('password is required')
    .notEmpty()
    .isLength({min: 8})
    .withMessage('password must contains 8 chars')
    .custom((value, { req }) => !!req.body.confirm_password)
    .withMessage('Please confirm your password'),
  body('confirm_password')
    .exists()
    .custom((value, {req}) => value == req.body.password)
    .withMessage('confirm_password field must have the same value as the password field')
];

const loginUserSchema = [
  body('email')
    .exists()
    .withMessage('email is required')
    .isEmail()
    .withMessage('Must be a valid email')
    .normalizeEmail(),
    body('pass')
    .exists()
    .withMessage('password is required')
    .notEmpty()
    .withMessage('password must be filled')
]

module.exports = {
  createUserSchema,
  updateUserSchema,
  loginUserSchema
}