const express = require('express');
const router = express.Router();
const masterInvitationTypeController = require('../controllers/masterInvitationType');
const { authCommon } = require('../middlewares/auth');
const roleUser = require('../utils/roleUser');
const awaitHandlerFactory = require('../middlewares/awaitHandlerFactory');
const {
  createMasterInvitationTypeSchema,
  updateMasterInvitationTypeSchema
} = require('../middlewares/validator/masterInvitationTypeValidator');

router.get('/', authCommon(), awaitHandlerFactory(masterInvitationTypeController.getAllMasterInvitationType));
router.get('/id/:id', authCommon(), awaitHandlerFactory(masterInvitationTypeController.getMasterInvitationTypeById));
router.get('/code/:invitation_type_code', authCommon(), awaitHandlerFactory(masterInvitationTypeController.getMasterInvitationTypeByCode));
router.post('/', authCommon(roleUser.admin), createMasterInvitationTypeSchema, awaitHandlerFactory(masterInvitationTypeController.createMasterInvitationType));
router.patch('/id/:id', authCommon(roleUser.admin), updateMasterInvitationTypeSchema, awaitHandlerFactory(masterInvitationTypeController.updateMasterInvitationType));
router.delete('/id/:id', authCommon(roleUser.admin), awaitHandlerFactory(masterInvitationTypeController.deleteMasterInvitationType));

module.exports = router;