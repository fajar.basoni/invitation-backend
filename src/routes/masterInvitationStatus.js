const express = require('express');
const router = express.Router();
const masterInvitationStatusController = require('../controllers/masterInvitationStatus');
const { authCommon } = require('../middlewares/auth');
const roleUser = require('../utils/roleUser');
const awaitHandlerFactory = require('../middlewares/awaitHandlerFactory');
const {
  createMasterInvitationStatusSchema,
  updateMasterInvitationStatusSchema
} = require('../middlewares/validator/masterInvitationStatusValidator');

router.get('/', authCommon(), awaitHandlerFactory(masterInvitationStatusController.getAllMasterInvitationStatus));
router.get('/id/:id', authCommon(), awaitHandlerFactory(masterInvitationStatusController.getMasterInvitationStatusById));
router.get('/code/:invitation_status_code', authCommon(), awaitHandlerFactory(masterInvitationStatusController.getMasterInvitationStatusByCode));
router.post('/', authCommon(roleUser.admin), createMasterInvitationStatusSchema, awaitHandlerFactory(masterInvitationStatusController.createMasterInvitationStatus));
router.patch('/id/:id', authCommon(roleUser.admin), updateMasterInvitationStatusSchema, awaitHandlerFactory(masterInvitationStatusController.updateMasterInvitationStatus));
router.delete('/id/:id', authCommon(roleUser.admin), awaitHandlerFactory(masterInvitationStatusController.deleteMasterInvitationStatus));

module.exports = router;