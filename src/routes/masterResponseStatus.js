const express = require('express');
const router = express.Router();
const masterResponseStatusController = require('../controllers/masterResponseStatus');
const { authCommon } = require('../middlewares/auth');
const roleUser = require('../utils/roleUser');
const awaitHandlerFactory = require('../middlewares/awaitHandlerFactory');
const {
  createMasterResponseStatusSchema,
  updateMasterResponseStatusSchema
} = require('../middlewares/validator/masterResponseStatusValidator');

router.get('/', authCommon(), awaitHandlerFactory(masterResponseStatusController.getAllMasterResponseStatus));
router.get('/id/:id', authCommon(), awaitHandlerFactory(masterResponseStatusController.getMasterResponseStatusById));
router.get('/code/:response_status_code', authCommon(), awaitHandlerFactory(masterResponseStatusController.getMasterResponseStatusByCode));
router.post('/', authCommon(roleUser.admin), createMasterResponseStatusSchema, awaitHandlerFactory(masterResponseStatusController.createMasterResponseStatus));
router.patch('/id/:id', authCommon(roleUser.admin), updateMasterResponseStatusSchema, awaitHandlerFactory(masterResponseStatusController.updateMasterResponseStatus));
router.delete('/id/:id', authCommon(roleUser.admin), awaitHandlerFactory(masterResponseStatusController.deleteMasterResponseStatus));

module.exports = router;