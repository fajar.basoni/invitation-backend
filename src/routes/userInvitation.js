const express = require('express');
const router = express.Router();
const userInvitationController = require('../controllers/userInvitation');
const {authCommon} = require('../middlewares/auth');
const roleUser = require('../utils/roleUser');
const awaitHandlerFactory = require('../middlewares/awaitHandlerFactory');
const {
  createUserInvitationSchema,
  updateUserInvitationSchema
} = require('../middlewares/validator/userInvitationValidator');

router.get('/', authCommon(), awaitHandlerFactory(userInvitationController.getAllUserInvitation));
router.get('/id/:id', authCommon(), awaitHandlerFactory(userInvitationController.getUserInvitationById));
router.post('/', authCommon(roleUser.admin), createUserInvitationSchema, awaitHandlerFactory(userInvitationController.createUserInvitation));
router.patch('/id/:id', authCommon(roleUser.admin), updateUserInvitationSchema, awaitHandlerFactory(userInvitationController.updateUserInvitation));
router.delete('/id/:id', authCommon(roleUser.admin), awaitHandlerFactory(userInvitationController.deleteUserInvitation));

module.exports = router;