const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');
const { auth } = require('../middlewares/auth');
const roleUser = require('../utils/roleUser');
const awaitHandlerFactory = require('../middlewares/awaitHandlerFactory');
const {
  createUserSchema,
  updateUserSchema,
  loginUserSchema
} = require('../middlewares/validator/userValidator');

router.get('/', auth(), awaitHandlerFactory(userController.getAllUsers));
router.get('/id/:id', auth(), awaitHandlerFactory(userController.getUserById));
router.get('/username/:username', auth(), awaitHandlerFactory(userController.getUserByUsername));
router.get('/whoami', auth(), awaitHandlerFactory(userController.getCurrentUser));
router.post('/', createUserSchema, awaitHandlerFactory(userController.createUser));
router.patch('/id/:id', auth(roleUser.admin), updateUserSchema, awaitHandlerFactory(userController.updateUser));
router.delete('/id/:id', auth(roleUser.admin), awaitHandlerFactory(userController.deleteUser));

router.post('/login', loginUserSchema, awaitHandlerFactory(userController.loginUser))

module.exports = router;