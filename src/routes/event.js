const express = require('express');
const router = express.Router();
const eventController = require('../controllers/event');
const {authCommon} = require('../middlewares/auth');
const roleUser = require('../utils/roleUser');
const awaitHandlerFactory = require('../middlewares/awaitHandlerFactory');

router.get('/', authCommon(), awaitHandlerFactory(eventController.getAllEvent));
router.get('/id/:id', authCommon(), awaitHandlerFactory(eventController.getEventById));
router.post('/', authCommon(roleUser.admin), awaitHandlerFactory(eventController.createEvent));
// router.patch('/id/:id', authCommon(roleUser.admin), updateUserInvitationSchema, awaitHandlerFactory(eventController.updateEvent));
router.delete('/id/:id', authCommon(roleUser.admin), awaitHandlerFactory(eventController.deleteEvent));

module.exports = router;