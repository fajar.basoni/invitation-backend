const eventModel = require('../models/event');
const eventGalleryModel = require('../models/eventGallery');
const eventInvitedModel = require('../models/eventInvited');
const eventLocationModel = require('../models/eventLocation');
const eventResponseModel = require('../models/eventResponse');
const eventStoryModel = require('../models/eventStory');

const httpExceptionUtils = require('../utils/httpException');
const { checkValidation, CommonMessage, generateText, generateActionUrl } = require('../utils/common');
const dotenv = require('dotenv');
dotenv.config();
const commonMessage = new CommonMessage;

class EventController {
  getAllEvent = async (req, res, next) => {
    let eventList = await eventModel.find();

    if (!eventList.length) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    eventList = eventList.map(event => {
      return event;
    });

    res.send(eventList);
  }

  getEventById = async (req, res, next) => {
    const event = await eventModel.findOne({ id: req.params.id });

    event["galleries"] = await eventGalleryModel.find({ id_event: req.params.id });
    event["invited"] = await eventInvitedModel.find({ id_event: req.params.id });
    event["locations"] = await eventLocationModel.find({ id_event: req.params.id });
    event["stories"] = await eventStoryModel.find({ id_event: req.params.id });
    
    if (!event) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(event);
  }

  createEvent = async (req, res, next) => {
    const {
      id_user,
      id_template,
      event_name,
      event_date,
      groom_name,
      bride_name,
      groom_parents,
      bride_parents
    } = req.body;
    
    const dataEvent = {
      id_user,
      id_template,
      event_name,
      event_date,
      groom_name,
      bride_name,
      groom_parents,
      bride_parents
    }

    const dataGalleries = req.body.galleries;
    const dataInvited = req.body.invited;
    const dataLocations = req.body.locations;
    const dataStories = req.body.stories;
    
    const result = await eventModel.create(dataEvent);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    let isExecuted;
    dataGalleries.map(gallery => {
      gallery["id_event"] = result.insertedId;
      isExecuted = eventGalleryModel.create(gallery);
      if (!isExecuted) {
        throw new httpExceptionUtils(500, commonMessage.Error);
      }
    });
    dataInvited.map(invited => {
      invited["id_event"] = result.insertedId;
      isExecuted = eventInvitedModel.create(invited);
      if (!isExecuted) {
        throw new httpExceptionUtils(500, commonMessage.Error);
      }
    });
    dataLocations.map(location => {
      location["id_event"] = result.insertedId;
      isExecuted = eventLocationModel.create(location);
      if (!isExecuted) {
        throw new httpExceptionUtils(500, commonMessage.Error);
      }
    });
    dataStories.map(story => {
      story["id_event"] = result.insertedId;
      isExecuted = eventStoryModel.create(story);
      if (!isExecuted) {
        throw new httpExceptionUtils(500, commonMessage.Error);
      }
    });

    res.status(201).send(commonMessage.Success);
  }

  deleteEvent = async (req, res, next) => {
    const result = await eventModel.delete(req.params.id);

    if (!result) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(commonMessage.Success);
  }
}

module.exports = new EventController;