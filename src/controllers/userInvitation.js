const userInvitationModel = require('../models/userInvitation');
const httpExceptionUtils = require('../utils/httpException');
const { checkValidation, CommonMessage, generateText, generateActionUrl } = require('../utils/common');
const dotenv = require('dotenv');
dotenv.config();
const commonMessage = new CommonMessage;

class UserInvitationController {
  getAllUserInvitation = async (req, res, next) => {
    let userInvitationList = await userInvitationModel.find();

    if (!userInvitationList.length) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    userInvitationList = userInvitationList.map(user_invitation => {
      return user_invitation;
    });

    res.send(userInvitationList);
  }

  getUserInvitationById = async (req, res, next) => {
    const userInvitation = await userInvitationModel.findOne({ id: req.params.id });

    if (!userInvitation) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(userInvitation);
  }

  createUserInvitation = async (req, res, next) => {
    req.body.text_message = await generateText(req.body);
    req.body.action_url = await generateActionUrl(req.body);
    checkValidation(req);
    const result = await userInvitationModel.create(req.body);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    res.status(201).send(commonMessage.Success);
  }

  updateUserInvitation = async (req, res, next) => {
    checkValidation(req);
    req.body.text_message = await generateText(req.body);
    req.body.action_url = await generateActionUrl(req.body);
    const { username, full_name, ...userInvitationWithotUsername } = req.body
    const result = await userInvitationModel.update(userInvitationWithotUsername, req.params.id);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    const { affectedRows, changedRows, info } = result;
    const message = !affectedRows ? commonMessage.NotFound : affectedRows && changedRows ? commonMessage.Success : commonMessage.Failed;

    res.send({ message, info });
  }

  deleteUserInvitation = async (req, res, next) => {
    const result = await userInvitationModel.delete(req.params.id);

    if (!result) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(commonMessage.Success);
  }
}

module.exports = new UserInvitationController;