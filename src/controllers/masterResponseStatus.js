const masterResponseStatusModel = require('../models/masterResponseStatus');
const httpExceptionUtils = require('../utils/httpException');
const { checkValidation, CommonMessage } = require('../utils/common');
const dotenv = require('dotenv');
dotenv.config();
const commonMessage = new CommonMessage;

class MasterResponseStatusController {
  getAllMasterResponseStatus = async (req, res, next) => {
    let masterResponseStatusList = await masterResponseStatusModel.find();

    if (!masterResponseStatusList.length) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    masterResponseStatusList = masterResponseStatusList.map(master_response_status => {
      return master_response_status;
    });

    res.send(masterResponseStatusList);
  }

  getMasterResponseStatusById = async (req, res, next) => {
    const masterResponseStatus = await masterResponseStatusModel.findOne({ id: req.params.id });

    if (!masterResponseStatus) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(masterResponseStatus);
  }

  getMasterResponseStatusByCode = async (req, res, next) => {
    const masterResponseStatus = await masterResponseStatusModel.findOne({ response_status_code: req.params.response_status_code });

    if (!masterResponseStatus) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(masterResponseStatus);
  }

  createMasterResponseStatus = async (req, res, next) => {
    checkValidation(req);
    const result = await masterResponseStatusModel.create(req.body);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    res.status(201).send('Status Was Created');
  }

  updateMasterResponseStatus = async (req, res, next) => {
    checkValidation(req);

    const result = await masterResponseStatusModel.update(req.body, req.params.id);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    const { affectedRows, changedRows, info } = result;
    const message = !affectedRows ? commonMessage.NotFound : affectedRows && changedRows ? commonMessage.Success : commonMessage.Failed;

    res.send({ message, info });
  }

  deleteMasterResponseStatus = async (req, res, next) => {
    const result = await masterResponseStatusModel.delete(req.params.id);

    if (!result) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(commonMessage.Success);
  }
}

module.exports = new MasterResponseStatusController;