const masterInvitationStatusModel = require('../models/masterInvitationStatus');
const httpExceptionUtils = require('../utils/httpException');
const { checkValidation, CommonMessage } = require('../utils/common');
const dotenv = require('dotenv');
dotenv.config();
const commonMessage = new CommonMessage;

class MasterInvitationStatusController {
  getAllMasterInvitationStatus = async (req, res, next) => {
    let masterInvitationStatusList = await masterInvitationStatusModel.find();

    if(!masterInvitationStatusList.length) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    masterInvitationStatusList = masterInvitationStatusList.map(master_invitation_status => {
      return master_invitation_status;
    });

    res.send(masterInvitationStatusList);
  }

  getMasterInvitationStatusById = async (req, res, next) => {
    const masterInvitationStatus = await masterInvitationStatusModel.findOne({ id: req.params.id });

    if (!masterInvitationStatus) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(masterInvitationStatus);
  }

  getMasterInvitationStatusByCode = async (req, res, next) => {
    const masterInvitationStatus = await masterInvitationStatusModel.findOne({ invitation_status_code: req.params.invitation_status_code });

    if (!masterInvitationStatus) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(masterInvitationStatus);
  }

  createMasterInvitationStatus = async (req, res, next) => {
    checkValidation(req);
    const result = await masterInvitationStatusModel.create(req.body);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    res.status(201).send('Status Was Created');
  }

  updateMasterInvitationStatus = async (req, res, next) => {
    checkValidation(req);

    const result = await masterInvitationStatusModel.update(req.body, req.params.id);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    const { affectedRows, changedRows, info } = result;
    const message = !affectedRows ? commonMessage.NotFound : affectedRows && changedRows ? commonMessage.Success : commonMessage.Failed;

    res.send({ message, info });
  }

  deleteMasterInvitationStatus = async (req, res, next) => {
    const result = await masterInvitationStatusModel.delete(req.params.id);

    if(!result) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(commonMessage.Success);
  }
}

module.exports = new MasterInvitationStatusController;