const userModel = require('../models/user');
const httpExceptionUtils = require('../utils/httpException');
const { checkValidation, hashPassword, CommonMessage } = require('../utils/common');
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
const commonMessage = new CommonMessage;

class UserController {
  getAllUsers = async (req, res, next) => {
    let userList = await userModel.find();

    if (!userList.length) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    userList = userList.map(user => {
      const { password, ...userWithoutPassword } = user;
      return userWithoutPassword;
    });

    res.send(userList);
  }

  getUserById = async (req, res, next) => {
    const user = await userModel.findOne({ id: req.params.id });

    if (!user) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    const { password, ...userWithoutPassword } = user;
    res.send(userWithoutPassword);
  }

  getUserByUsername = async (req, res, next) => {
    const user = await userModel.findOne({ username: req.params.username });

    if (!user) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    const { password, ...userWithoutPassword } = user;
    res.send(userWithoutPassword);
  }

  getCurrentUser = async (req, res, next) => {
    const { password, ...userWithoutPassword } = req.currentUser;

    res.send(userWithoutPassword);
  }

  createUser = async (req, res, next) => {
    checkValidation(req);
    await hashPassword(req);
    const result = await userModel.create(req.body);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    res.status(201).send(commonMessage.Success);
  }

  updateUser = async (req, res, next) => {
    checkValidation(req);
    await hashPassword(req);

    const { confirm_password, ...restOfUpdates } = req.body;
    const result = await userModel.update(restOfUpdates, req.params.id);

    if (!result) {
      throw new httpExceptionUtils(404, commonMessage.Error);
    }

    const { affectedRows, changedRows, info } = result;
    const message = !affectedRows ? commonMessage.NotFound : affectedRows && changedRows ? commonMessage.Success : commonMessage.Failed;

    res.send({ message, info });
  }

  deleteUser = async (req, res, next) => {
    const result = await userModel.delete(req.params.id);

    if(!result) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(commonMessage.Success);
  }

  loginUser = async (req, res, next) => {
    checkValidation(req);
    const {email, pass} = req.body;
    const user = await userModel.findOne({email});

    if(!user) {
      throw new httpExceptionUtils(401, commonMessage.UnableLogin);
    }

    const isMatch = await bcryptjs.compare(pass, user.password);

    if(!isMatch) {
      throw new httpExceptionUtils(401, commonMessage.IncorrectPassword);
    }

    const secretKey = process.env.SECRET_JWT || "";
    const token = jwt.sign({ id : user.id.toString() }, secretKey, {
      expiresIn: '24h'
    });

    const {password, ...userWithoutPassword} = user;

    res.send({userWithoutPassword, token});
  }
}

module.exports = new UserController;