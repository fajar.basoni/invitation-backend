const masterInvitationTypeModel = require('../models/masterInvitationType');
const httpExceptionUtils = require('../utils/httpException');
const { checkValidation, CommonMessage } = require('../utils/common');
const dotenv = require('dotenv');
dotenv.config();
const commonMessage = new CommonMessage;

class MasterInvitationTypeController {
  getAllMasterInvitationType = async (req, res, next) => {
    let masterInvitationTypeList = await masterInvitationTypeModel.find();

    if (!masterInvitationTypeList.length) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    masterInvitationTypeList = masterInvitationTypeList.map(master_invitation_type => {
      return master_invitation_type;
    });

    res.send(masterInvitationTypeList);
  }

  getMasterInvitationTypeById = async (req, res, next) => {
    const masterInvitationType = await masterInvitationTypeModel.findOne({ id: req.params.id });

    if (!masterInvitationType) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(masterInvitationType);
  }

  getMasterInvitationTypeByCode = async (req, res, next) => {
    const masterInvitationType = await masterInvitationTypeModel.findOne({ invitation_type_code: req.params.invitation_type_code });

    if (!masterInvitationType) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(masterInvitationType);
  }

  createMasterInvitationType = async (req, res, next) => {
    checkValidation(req);
    const result = await masterInvitationTypeModel.create(req.body);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    res.status(201).send('Status Was Created');
  }

  updateMasterInvitationType = async (req, res, next) => {
    checkValidation(req);

    const result = await masterInvitationTypeModel.update(req.body, req.params.id);

    if (!result) {
      throw new httpExceptionUtils(500, commonMessage.Error);
    }

    const { affectedRows, changedRows, info } = result;
    const message = !affectedRows ? commonMessage.NotFound : affectedRows && changedRows ? commonMessage.Success : commonMessage.Failed;

    res.send({ message, info });
  }

  deleteMasterInvitationType = async (req, res, next) => {
    const result = await masterInvitationTypeModel.delete(req.params.id);

    if (!result) {
      throw new httpExceptionUtils(404, commonMessage.NotFound);
    }

    res.send(commonMessage.Success);
  }
}

module.exports = new MasterInvitationTypeController;