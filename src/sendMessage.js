// https://api.whatsapp.com/send?phone=+{{ *YOURNUMBER* }}&text=%20{{ *YOUR MESSAGE* }}

var yourNumber = "+6281296463749"
var yourMessage = "please, check your invitation in this link : https://google.com"

// %20 mean space in link
// If you already had an array then you just join them with '%20'
// easy right

function sendMessageWa(number, message) {
  var url = `https://api.whatsapp.com/send?phone=${number}&text=${encodeURIComponent(message)}`;
  console.log(url)
  return url
}

function sendMessageEmail(emailAddress, emailSubject, emailBody) {
  var url = `mailto:${emailAddress}?subject=${emailSubject}&body=${encodeURIComponent(emailBody)}`;
  console.log(url)
  return url
}

sendMessageWa(yourNumber, yourMessage)